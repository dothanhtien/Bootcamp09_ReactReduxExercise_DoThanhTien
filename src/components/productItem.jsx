import React, { Component } from "react";
import { connect } from "react-redux";

class ProductItem extends Component {
  setSelectedProduct = () => {
    this.props.dispatch({
      type: "SET_SELECTED_PRODUCT",
      payload: this.props.item,
    });
  };

  addToCart = () => {
    const cartItem = {
      product: this.props.item,
      quantity: 1,
    };

    this.props.dispatch({
      type: "ADD_TO_CART",
      payload: cartItem,
    });
  };

  render() {
    const { img, name, price } = this.props.item;
    return (
      <div className="col-sm-6 col-lg-3 mb-sm-3 mb-lg-0">
        <div className="card">
          <img
            src={img}
            className="card-img-top"
            alt="product"
            style={{ maxHeight: 250 }}
          />
          <div className="card-body">
            <h5 className="card-title">{name}</h5>
            <p className="card-text">${price}</p>
            <button
              className="btn btn-primary w-100 mb-3"
              onClick={this.setSelectedProduct}
            >
              Xem chi tiết
            </button>
            <button className="btn btn-success w-100" onClick={this.addToCart}>
              Thêm giỏ hàng
            </button>
          </div>
        </div>
      </div>
    );
  }
}

export default connect()(ProductItem);
