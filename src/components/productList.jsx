import React, { Component } from "react";
import { connect } from "react-redux";

import ProductItem from "./productItem";

class ProductList extends Component {
  renderProductList = () => {
    return this.props.products.map((item) => {
      return <ProductItem key={item.id} item={item} />;
    });
  };

  render() {
    return <div className="row mb-5">{this.renderProductList()}</div>;
  }
}

const mapStateToProps = (state) => {
  return { products: state.product.products };
};

export default connect(mapStateToProps, null)(ProductList);
