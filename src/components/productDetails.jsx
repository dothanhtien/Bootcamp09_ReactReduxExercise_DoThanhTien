import React, { Component } from "react";
import { connect } from "react-redux";

class ProductDetails extends Component {
  render() {
    return (
      <>
        {this.props.selectedProduct && (
          <div className="row mb-5">
            <div className="col-sm-3">
              <img
                src={this.props.selectedProduct.img}
                className="img-fluid"
                alt="product details"
              />
            </div>
            <div className="col-sm-9">
              <table className="table">
                <thead>
                  <tr>
                    <th scope="col" colSpan={2}>
                      <h3>Thông số kỹ thuật</h3>
                    </th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>Tên</td>
                    <td>{this.props.selectedProduct.name}</td>
                  </tr>
                  <tr>
                    <td>Màn hình</td>
                    <td>{this.props.selectedProduct.screen}</td>
                  </tr>
                  <tr>
                    <td>Camera trước</td>
                    <td>{this.props.selectedProduct.frontCamera}</td>
                  </tr>
                  <tr>
                    <td>Camera sau</td>
                    <td>{this.props.selectedProduct.backCamera}</td>
                  </tr>
                  <tr>
                    <td>Mô tả</td>
                    <td>{this.props.selectedProduct.desc}</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        )}
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return { selectedProduct: state.product.selectedProduct };
};

export default connect(mapStateToProps, null)(ProductDetails);
