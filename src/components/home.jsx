import React, { Component } from "react";

import ProductList from "./productList";
import ProductDetails from "./productDetails";
import Cart from "./cart";

class Home extends Component {
  render() {
    return (
      <div className="container">
        <h1 className="text-center my-5">Shopping Online</h1>
        <div className="text-center mb-5">
          <button
            className="btn btn-success"
            data-toggle="modal"
            data-target="#cartModal"
          >
            Giỏ hàng
          </button>
        </div>
        <ProductList />
        <ProductDetails />
        <Cart />
      </div>
    );
  }
}

export default Home;
