import React, { Component } from "react";
import { connect } from "react-redux";

class Cart extends Component {
  increaseQuantity = (id) => {
    this.props.dispatch({
      type: "INCREASE_QUANTITY",
      payload: id,
    });
  };

  decreaseQuantity = (id) => {
    this.props.dispatch({
      type: "DECREASE_QUANTITY",
      payload: id,
    });
  };

  removeFromCart = (id) => {
    this.props.dispatch({
      type: "REMOVE_FROM_CART",
      payload: id,
    });
  };

  renderCartItems = () => {
    return this.props.cart.map((item) => {
      return (
        <tr key={item.product.id}>
          <th scope="row">{item.product.id}</th>
          <td>
            <img
              src={item.product.img}
              className="img-fluid"
              style={{ maxHeight: 100 }}
              alt="cart item"
            />
          </td>
          <td>{item.product.name}</td>
          <td>
            <button
              className="btn btn-info btn-sm"
              onClick={() => this.decreaseQuantity(item.product.id)}
            >
              -
            </button>
            <span> {item.quantity} </span>
            <button
              className="btn btn-info btn-sm"
              onClick={() => this.increaseQuantity(item.product.id)}
            >
              +
            </button>
          </td>
          <td>${item.product.price}</td>
          <td>${item.product.price * item.quantity}</td>
          <td>
            <button
              className="btn btn-danger btn-sm"
              onClick={() => this.removeFromCart(item.product.id)}
            >
              Xóa
            </button>
          </td>
        </tr>
      );
    });
  };

  render() {
    return (
      <div
        className="modal fade"
        id="cartModal"
        tabIndex={-1}
        aria-labelledby="cartModalLabel"
        aria-hidden="true"
      >
        <div className="modal-dialog modal-lg">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="cartModalLabel">
                Giỏ hàng
              </h5>
              <button
                type="button"
                className="close"
                data-dismiss="modal"
                aria-label="Close"
              >
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <div className="modal-body">
              <div className="table-responsive">
                <table className="table">
                  <thead>
                    <tr>
                      <th scope="col">Mã sản phẩm</th>
                      <th scope="col">Hình ảnh</th>
                      <th scope="col">Tên sản phẩm</th>
                      <th scope="col">Số lượng</th>
                      <th scope="col">Đơn giá</th>
                      <th scope="col">Thành tiền</th>
                      <th scope="col"></th>
                    </tr>
                  </thead>
                  <tbody>
                    {!this.props.cart.length && (
                      <tr>
                        <td className="text-center font-italic" colSpan={7}>
                          Không có sản phẩm nào trong giỏ hàng
                        </td>
                      </tr>
                    )}
                    {this.renderCartItems()}
                  </tbody>
                </table>
              </div>
            </div>
            <div className="modal-footer">
              <button
                type="button"
                className="btn btn-secondary"
                data-dismiss="modal"
              >
                Đóng
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return { cart: state.cart.cart };
};

export default connect(mapStateToProps, null)(Cart);
