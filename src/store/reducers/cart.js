const initialState = {
  cart: [],
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case "ADD_TO_CART": {
      const cloneCart = [...state.cart];

      const foundIndex = cloneCart.findIndex((item) => {
        return item.product.id === action.payload.product.id;
      });

      if (foundIndex === -1) {
        cloneCart.push(action.payload);
      } else {
        cloneCart[foundIndex].quantity++;
      }

      state.cart = cloneCart;
      return { ...state };
    }
    case "INCREASE_QUANTITY": {
      const cloneCart = [...state.cart];

      const foundIndex = cloneCart.findIndex((item) => {
        return item.product.id === action.payload;
      });

      cloneCart[foundIndex].quantity++;

      state.cart = cloneCart;
      return { ...state };
    }
    case "DECREASE_QUANTITY": {
      const cloneCart = [...state.cart];

      const foundIndex = cloneCart.findIndex((item) => {
        return item.product.id === action.payload;
      });

      cloneCart[foundIndex].quantity--;

      if (!cloneCart[foundIndex].quantity) {
        cloneCart.splice(foundIndex, 1);
      }

      state.cart = cloneCart;
      return { ...state };
    }
    case "REMOVE_FROM_CART": {
      const cloneCart = [...state.cart];

      const foundIndex = cloneCart.findIndex((item) => {
        return item.product.id === action.payload;
      });

      cloneCart.splice(foundIndex, 1);

      state.cart = cloneCart;
      return { ...state };
    }
    default:
      return state;
  }
};

export default reducer;
